<?php


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$locale = isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE']) : 'ru';
// var_dump(mb_strtolower($locale));
// echo '<div></div>';
// var_dump(in_array(mb_strtolower($locale), ['pl', 'pl-pl',]));

// die();
if (in_array(mb_strtolower($locale), ['pl', 'pl-pl', 'pl_pl'])) {
    $url_redirect = '/pl';
    if (isset($_SERVER['SCRIPT_NAME'])) {
        $url_redirect = str_replace('/index.php', '', $_SERVER['SCRIPT_NAME']).$url_redirect;
    }
    header('Location: '.$url_redirect);
}

include 'index.html';
